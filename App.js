/**
 * The Playground
 * ---------------------
 * A Place to Test Code Quickly & Easily.
 */

/**
 * NOTE:
 * This works on React Natigation & A Stack Navigator.
 * Define a new Elment to be added to the list view in the router file.
 */


import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Theme from './Themes/Default';

import AppNavigator from './config/routes';

export default class App extends React.Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: Theme.BaseColor,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
