const Theme = {
  BaseColor: 'white',
  PrimaryColor: '#3acfa7',
  OffColor: '#CCCCCC',
  shadowColor: "purple",
  shadowOffset: {
      width: 0,
      height: 7.5
  },
  shadowOpacity: 0.1,
  shadowRadius: 10,
  padding: 5
}

export default Theme;
