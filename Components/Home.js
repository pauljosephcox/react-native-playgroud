import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Theme from '../Themes/Default';
// import Config from '../config/config';

import PlaygroundsListView from './PlaygroundsListView';

export default class Home extends React.Component {

  static navigationOptions = { title: "The Playground" }

  // goTo(screen){
  //   this.props.navigation.navigate(screen)
  // }


  render() {
    return (
      <View style={styles.container}>
        <PlaygroundsListView {...this.props} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Theme.BaseColor,
    padding: 5

  },
});
