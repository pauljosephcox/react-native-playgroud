import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Theme from '../Themes/Default';

export default class ExampleTwo extends React.Component {

  static navigationOptions = { title: "My Other Example" }

  constructor(props){
    super(props)
  }


  render() {
    return (
      <View style={styles.container}>
        <Text> Example 2 </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Theme.PrimaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
