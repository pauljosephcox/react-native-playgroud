import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Theme from '../Themes/Default';


// Import Playgrounds
import Example from './Example';
import ExampleTwo from './ExampleTwo';
import AlertExample from './AlertExample';
import FeedExample from './FeedExample';
import ImageExample from './ImageExample';
import SearchExample from './SearchExample';
import StorageExample from './StorageExample';
import WebViewExample from './WebViewExample';

// Dyncamically Render
function DynamicRender(props){

  const detailsCom = props.name;

  if(detailsCom == 'Example') return <Example />;
  else if(detailsCom == 'ExampleTwo') return <ExampleTwo />;
  else if(detailsCom == 'AlertExample') return <AlertExample />;
  else if(detailsCom == 'FeedExample') return <FeedExample />;
  else if(detailsCom == 'ImageExample') return <ImageExample />;
  else if(detailsCom == 'SearchExample') return <SearchExample />;
  else if(detailsCom == 'StorageExample') return <StorageExample />;
  else if(detailsCom == 'WebViewExample') return <WebViewExample />;

}

export default class Play extends React.Component {

  // Set Navigation Title
  static navigationOptions = ({navigation}) => ({
    title: navigation.state.params.comp,
  });

  constructor(props){
    super(props)
  }



  render() {

    const detailsTitle = this.props.navigation.getParam('title');
    const detailsCom = this.props.navigation.getParam('comp');

    return (
      <View style={styles.container}>

        <DynamicRender name={detailsCom} />

      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
