import React from 'react';
import { ListView, View, Text, StyleSheet, Alert, TouchableOpacity } from 'react-native';
import Theme from '../Themes/Default';

export default class PlaygroundsListView extends React.Component {

  constructor(props){
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      text: "TESTING....",
      dataSource: ds.cloneWithRows([
        { title: 'My First Playground', comp: 'Example'},
        { title: 'My Second Playground', comp: 'ExampleTwo'},
        { title: 'Alert Example', comp: 'AlertExample'},
        { title: 'Feed Fetch Example', comp: 'FeedExample'},
        { title: 'Image Example', comp: 'ImageExample'},
        { title: 'Search Example', comp: 'SearchExample'},
        { title: 'Storage Example', comp: 'StorageExample'},
        { title: 'Webview Example', comp: 'WebViewExample'}
      ]),
    }
  }

  onRowPress(data){

    // Alert.alert("Hello");
    this.props.navigation.navigate('Play',data)

  }

  render() {
    return (
      <ListView style={styles.container} dataSource={this.state.dataSource} renderRow={(data)=><PlaygroundRow onPress={this.onRowPress.bind(this,data)} {...data} />} />
    );
  }
}

const PlaygroundRow = (props) => (

  <TouchableOpacity style={styles.row} onPress={props.onPress}>
    <Text>{props.title}</Text>
  </TouchableOpacity>

);

const styles = StyleSheet.create({

  container: {

    flex: 1,

  },

  row: {
    padding: 20,
    paddingTop: 40,
    paddingBottom: 40,
    borderColor: Theme.OffColor,
    borderWidth: 0,
    borderRadius: 5,
    backgroundColor: "white",
    margin: 10,
    shadowColor: Theme.shadowColor,
    shadowOffset: Theme.shadowOffset,
    shadowOpacity: Theme.shadowOpacity,
    shadowRadius: Theme.shadowRadius
  }

});
