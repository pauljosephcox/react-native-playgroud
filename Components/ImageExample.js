import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';
import Theme from '../Themes/Default';

export default class ImageExample extends React.Component {

  constructor(props){
    super(props)
  }


  render() {
    return (
        <View style={styles.container}>

            <Image resizeMode="cover" style={styles.image} source={{uri: 'https://c.wallhere.com/photos/6b/20/digital_art_portrait_display_low_poly_gradient_3d_object_simple_minimalism-71608.png!d'}} />
            <Text style={styles.text}>My Title</Text>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Theme.BaseColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
      fontSize: 20,
      color: "#000"
  },
  image: {
      position: 'absolute',
      top: 0,
      left:0,
      right: 0,
      bottom: 0
  }
});
