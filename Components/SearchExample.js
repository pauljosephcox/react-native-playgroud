import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import Theme from '../Themes/Default';

export default class Example extends React.Component {

  static navigationOptions = { title: "My Example" }

  constructor(props){
    super(props)
    this.state = { text: '' };
  }


  render() {
    return (
      <View style={styles.container}>
        <Text> SEARCH EXAMPLE </Text>
        <Text style={styles.title}>{this.state.text}</Text>
        <TextInput autoFocus={true} value={this.state.text} style={styles.input} onChangeText={(text)=>this.setState({text})}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Theme.BaseColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
      height: 40,
      borderWidth: 1,
      borderColor: Theme.OffColor,
      width: 200
  },
  title: {
      fontFamily: 'Zapfino',
      fontSize: 30,
      fontWeight: 'bold',
      color: Theme.PrimaryColor
  }
});
