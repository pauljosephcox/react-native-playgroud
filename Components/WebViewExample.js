import React from 'react';
import { StyleSheet, WebView } from 'react-native';
import Theme from '../Themes/Default';

export default class WebViewExample extends React.Component {

  static navigationOptions = { title: "My Example" }

  constructor(props){
    super(props)
  }


  render() {
    return (
      <WebView style={styles.container} source={{uri: 'https://sportsrecruits.com'}} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Theme.BaseColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
