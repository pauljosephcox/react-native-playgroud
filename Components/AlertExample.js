import React from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import Theme from '../Themes/Default';

export default class AlertExample extends React.Component {

  constructor(props){

    super(props)

    this.handleOnPress = this.handleOnPress.bind(this);

  }

  handleOnPress(str){
      Alert.alert(str);
  }


  render() {
    return (
      <View style={styles.container}>
        <Text> Alert Example </Text>
        <Button style={styles.button} title="Press This Button" onPress={() => this.handleOnPress("Hello React!!!")}></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Theme.BaseColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
      padding: Theme.padding,
      backgroundColor: Theme.PrimaryColor,
      color: "#FFFFFF"
  }
});
