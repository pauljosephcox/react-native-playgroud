import React from 'react';
import { StyleSheet, Text, View, Button, ListView, ActivityIndicator } from 'react-native';
import Theme from '../Themes/Default';

export default class FeedExample extends React.Component {

  constructor(props){

    super(props)

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state ={
        isLoading: true,
        dataSource: ds.cloneWithRows([])
    }



  }

  componentDidMount(){

      fetch('https://facebook.github.io/react-native/movies.json')
        .then(response => response.json())
        .then(json =>{
            this.setState({
              isLoading: false,
              dataSource: this.state.dataSource.cloneWithRows(json.movies),
          })
        })

  }

  render() {

      if(this.state.isLoading){
       return(
         <View style={{flex: 1, padding: 20}}>
           <ActivityIndicator/>
         </View>
       )
     }

    return (

            <ListView style={styles.container} dataSource={this.state.dataSource} renderRow={(data)=><FeedRow {...data} />} />

    );
  }
}

const FeedRow = (props) => (

  <View style={styles.row}>
    <Text>{props.title} | {props.releaseYear}</Text>
  </View>

);

const styles = StyleSheet.create({

  container: {

    flex: 1,
    backgroundColor: "white"

  },

  row: {
    padding: 20,
    paddingTop: 40,
    paddingBottom: 40,
    borderColor: Theme.OffColor,
    borderWidth: 0,
    borderRadius: 5,
    backgroundColor: "white",
    margin: 10,
    shadowColor: Theme.shadowColor,
    shadowOffset: Theme.shadowOffset,
    shadowOpacity: Theme.shadowOpacity,
    shadowRadius: Theme.shadowRadius
  }

});
