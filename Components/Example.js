import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Theme from '../Themes/Default';

export default class Example extends React.Component {

  static navigationOptions = { title: "My Example" }

  constructor(props){
    super(props)
  }


  render() {
    return (
      <View style={styles.container}>
        <Text> THIS IS AN EXAMPLE </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Theme.BaseColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
