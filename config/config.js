const config = {
  playgrounds: [
    'My first Playground',
    'My Second Playground',
    'My Third Playground',
    'Alert Example'
  ]
};

export default config;
