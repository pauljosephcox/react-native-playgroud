import React from 'react';
import { createStackNavigator } from 'react-navigation';

// Screens
import Home from '../Components/Home';
import Play from '../Components/Play';

export default AppNavigator = createStackNavigator(
  {
    Home: {
      screen: Home,
      title: "The Playground"
    },
    Play: {
      screen: Play,
      title: "Starting Playing"
    }
  },
  {
    initialRouteName: 'Home'
  }
);
